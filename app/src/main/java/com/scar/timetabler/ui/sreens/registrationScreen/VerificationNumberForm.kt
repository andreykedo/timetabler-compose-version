package com.scar.timetabler.ui.sreens.registrationScreen

import android.content.res.Configuration
import androidx.compose.foundation.layout.Column
import androidx.compose.material.Surface
import androidx.compose.material.TextField
import androidx.compose.material.TextFieldColors
import androidx.compose.material.TextFieldDefaults
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import com.scar.timetabler.ui.theme.TimeTablerTheme

@Composable
fun VerificationNumberForm(modifier: Modifier = Modifier) {
    TextField(
        value = "",
        modifier = modifier,
        onValueChange = {}
    )
}

@Composable
private fun View(){
    TimeTablerTheme {
        Surface {
            VerificationNumberForm()
        }
    }
}

@Preview
@Composable
private fun Light() = View()

@Preview(uiMode = Configuration.UI_MODE_NIGHT_YES)
@Composable
private fun Night() = View()