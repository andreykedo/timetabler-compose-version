package com.scar.timetabler.ui.sreens.registrationScreen

import android.content.res.Configuration.UI_MODE_NIGHT_YES
import androidx.compose.foundation.layout.*
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.input.PasswordVisualTransformation
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.scar.timetabler.ui.theme.TimeTablerTheme

@Composable
fun PasswordEditForm(modifier: Modifier = Modifier, onBack : () -> Unit = {}){
    Column(modifier) {
        TextField(
            value = "",
            shape = MaterialTheme.shapes.large,
            placeholder = { Text("Новый пароль") },
            onValueChange = { },
            visualTransformation = PasswordVisualTransformation(),
            modifier = Modifier.fillMaxWidth()
        )
        Spacer(Modifier.size(height = 12.dp, width = 0.dp))
        TextField(
            value = "",
            shape = MaterialTheme.shapes.large,
            placeholder = { Text("Повторите пароль") },
            onValueChange = { },
            visualTransformation = PasswordVisualTransformation(),
            modifier = Modifier.fillMaxWidth()
        )
        Spacer(Modifier.size(height = 14.dp, width = 0.dp))
        Row(modifier = Modifier.fillMaxWidth(), horizontalArrangement = Arrangement.SpaceBetween){
            TextButton(onClick = onBack) {
                Text("Назад")
            }
            Button(onClick = { /*TODO*/ }) {
                Text("Подтвердить")
            }
        }
    }
}

@Composable
private fun View(){
    TimeTablerTheme {
        Surface {
            PasswordEditForm()
        }
    }
}

@Preview
@Composable
private fun Light() = View()

@Preview(uiMode = UI_MODE_NIGHT_YES)
@Composable
private fun Night() = View()
