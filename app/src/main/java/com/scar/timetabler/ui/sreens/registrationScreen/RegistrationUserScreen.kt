package com.scar.timetabler.ui.sreens.registrationScreen

import android.content.res.Configuration.UI_MODE_NIGHT_YES
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.scar.timetabler.ui.theme.TimeTablerTheme
import com.scar.timetabler.ui.until.*

private val steps = arrayOf(
    Step(
        "Регистрация",
        "Введите ваши учётные данные"
    ),
    Step(
        "Придумайте пароль",
        "Придумайте надёжный пароль для вашей учетной записи"
    ),
    Step(
        "Подтверждение",
        "Введите код из СМС"
    )
)

@Composable
fun RegistrationUserScreen(){
    var stepState by remember(steps) {
        mutableStateOf(Snapshot(step = steps[0]))
    }
    Surface(color = MaterialTheme.colors.primary){
        Column(
            Modifier.fillMaxSize(),
            horizontalAlignment = Alignment.CenterHorizontally
        ) {
            Stepper(
                Modifier.padding(horizontal = 24.dp),
                steps,
                stepState
            ){ value ->
                stepState = value
            }
            Surface(
                Modifier.fillMaxSize(),
                shape = RoundedCornerShape(topStart = 24.dp, topEnd = 24.dp)
            ) {
                Column {
                    CrossSlide(
                        stepState,
                        reverseAnimation = stepState.isBack()
                    ) {
                        StepHeader(Modifier.padding(top = 14.dp, start = 14.dp), it.step)
                    }
                    CrossSlide(
                        stepState,
                        Modifier.fillMaxSize().padding(horizontal = 14.dp),
                        reverseAnimation = stepState.isBack()
                    ) { snapshot ->
                        Column(
                            Modifier.fillMaxSize(),
                            horizontalAlignment = Alignment.CenterHorizontally,
                            verticalArrangement = Arrangement.SpaceEvenly
                        ) {
                            when(snapshot.nextIndex){
                                0 -> DataInputForm{
                                    stepState = Snapshot(0, 1, steps[1])
                                }
                                1 -> PasswordEditForm{
                                    stepState = Snapshot(-1, 0, steps[0])
                                }
                                2 -> VerificationNumberForm()
                            }
                            TextButton(onClick = { /*TODO*/ }) {
                                Text("Есть аккаунт?", style = MaterialTheme.typography.caption)
                            }
                        }
                    }
                }
            }
        }
    }
}

@Composable
private fun StepHeader(modifier: Modifier = Modifier, value: Step){
    Column(modifier) {
        Text(
            value.title,
            style = MaterialTheme.typography.h4,
            fontWeight = FontWeight.Bold,
            color = MaterialTheme.colors.secondary)
        Spacer(modifier = Modifier.height(12.dp))
        Text(
            value.caption,
            style = MaterialTheme.typography.caption,
            color = MaterialTheme.colors.onBackground)
    }
}
@Composable
private fun View(){
    TimeTablerTheme {
        RegistrationUserScreen()
    }
}

@Preview
@Composable
private fun Light() = View()

@Preview(uiMode = UI_MODE_NIGHT_YES)
@Composable
private fun Night() = View()
