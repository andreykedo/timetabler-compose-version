package com.scar.timetabler.ui.sreens.registrationScreen

import android.content.res.Configuration.UI_MODE_NIGHT_YES
import androidx.compose.foundation.layout.*
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.style.TextDecoration
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.scar.timetabler.ui.theme.TimeTablerTheme

@Composable
fun DataInputForm(modifier: Modifier = Modifier, nextStep : () -> Unit = {}){
    val phone = remember {
        mutableStateOf("")
    }
    Column(modifier) {
        TextField(
            value = phone.value,
            shape = MaterialTheme.shapes.large,
            placeholder = { Text("Телефон") },
            onValueChange = {value -> phone.value = value},
            modifier = Modifier.fillMaxWidth()
        )
        Spacer(modifier = Modifier.height(15.dp))
        TextField(
            value = phone.value,
            shape = MaterialTheme.shapes.large,
            placeholder = { Text("EMail") },
            onValueChange = {value -> phone.value = value},
            modifier = Modifier.fillMaxWidth()
        )
        Spacer(modifier = Modifier.height(15.dp))
        TextField(
            value = phone.value,
            shape = MaterialTheme.shapes.large,
            placeholder = { Text("Фамилия") },
            onValueChange = {value -> phone.value = value},
            modifier = Modifier.fillMaxWidth()
        )
        Spacer(modifier = Modifier.height(15.dp))
        TextField(
            value = phone.value,
            shape = MaterialTheme.shapes.large,
            placeholder = { Text("Имя") },
            onValueChange = {value -> phone.value = value},
            modifier = Modifier.fillMaxWidth()
        )
        Spacer(modifier = Modifier.height(15.dp))
        TextField(
            value = phone.value,
            shape = MaterialTheme.shapes.large,
            placeholder = { Text("Дата рождения") },
            onValueChange = {value -> phone.value = value},
            modifier = Modifier.fillMaxWidth()
        )
        Spacer(Modifier.size(height = 23.dp, width = 0.dp))
        Button(onClick = nextStep, modifier = Modifier.fillMaxWidth()) {
            Text("Продолжить")
        }
        Spacer(Modifier.size(height = 14.dp, width = 0.dp))
        Row(modifier = Modifier.align(Alignment.CenterHorizontally),
            verticalAlignment = Alignment.CenterVertically) {
            Checkbox(checked = true, onCheckedChange = {})
            Spacer(Modifier.size(height = 0.dp, width = 4.dp))
            Text(
                "Согласие на обработку персональных данных",
                style = MaterialTheme.typography.caption,
                textDecoration = TextDecoration.Underline,
            )
        }
    }
}

@Composable
private fun View(){
    TimeTablerTheme {
        Surface {
            DataInputForm()
        }
    }
}

@Preview
@Composable
private fun DataInputView() = View()

@Preview(uiMode = UI_MODE_NIGHT_YES)
@Composable
private fun DataInputViewNight() = View()