package com.scar.timetabler.ui.theme

import androidx.compose.ui.graphics.Color

val primary = Color(0xFF2F4C79)
val primaryVariant = Color(0xFF191919)
val secondary = Color(0xFFFF6F3C)
//val background = Color(0xFFECDBBA)
