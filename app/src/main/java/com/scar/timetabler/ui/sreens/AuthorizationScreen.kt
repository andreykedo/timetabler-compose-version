package com.scar.timetabler.ui.sreens

import android.content.res.Configuration.UI_MODE_NIGHT_YES
import androidx.compose.foundation.layout.*
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.setValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.AnnotatedString
import androidx.compose.ui.text.SpanStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.input.PasswordVisualTransformation
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.scar.timetabler.ui.theme.TimeTablerTheme

private data class AuthData(val login: String, val password: String)

@Composable
fun AuthorizationScreen(){
    Surface {
        Column(
            Modifier.fillMaxSize(),
            verticalArrangement = Arrangement.SpaceEvenly,
            horizontalAlignment = Alignment.CenterHorizontally) {
            Text(AnnotatedString(
                "Online система образования\n\n").plus(AnnotatedString("Привет!", spanStyle = SpanStyle(fontWeight = FontWeight.Bold, color = MaterialTheme.colors.secondary))),
                style = MaterialTheme.typography.h4,
                textAlign = TextAlign.Center)
            Column(Modifier.fillMaxWidth(0.8f), horizontalAlignment = Alignment.CenterHorizontally) {
                AuthorizationForm()
                AdditionalFeature()
            }
            Text("Powered by S.C.A.R.©")
        }
    }
}

@Composable
private fun ColumnScope.AuthorizationForm(){
    var authDataState by remember {
        mutableStateOf(AuthData("",""))
    }
    OutlinedTextField(
        value = authDataState.login,
        modifier = Modifier.fillMaxWidth(),
        placeholder = { Text("Телефон")},
        onValueChange = { login -> authDataState = authDataState.copy(login = login)},
        maxLines = 1
    )
    Spacer(Modifier.size(height = 12.dp, width = 0.dp))
    OutlinedTextField(
        value = authDataState.password,
        modifier = Modifier.fillMaxWidth(),
        placeholder = { Text("Пароль") },
        onValueChange = { pwd -> authDataState = authDataState.copy(password = pwd)},
        visualTransformation = PasswordVisualTransformation(),
        maxLines = 1
    )
    Spacer(Modifier.size(height = 20.dp, width = 0.dp))
    Button(
        onClick = {  },
        modifier = Modifier
            .align(Alignment.CenterHorizontally)
            .fillMaxWidth()) {
        Text("Войти")
    }
}

@Composable
private fun AdditionalFeature(){
    Text("или", Modifier.padding(vertical = 12.dp))
    OutlinedButton(onClick = { /*TODO*/ }, Modifier.fillMaxWidth()) {
        Text("Восстановить пароль")
    }
    Spacer(Modifier.size(height = 12.dp, width = 0.dp))
    OutlinedButton(onClick = { /*TODO*/ }, Modifier.fillMaxWidth()) {
        Text("Нет аккаунта?")
    }
}


@Composable
private fun View(){
    TimeTablerTheme {
        AuthorizationScreen()
    }
}

@Preview
@Composable
private fun Light() = View()

@Preview(uiMode = UI_MODE_NIGHT_YES)
@Composable
private fun Night() = View()