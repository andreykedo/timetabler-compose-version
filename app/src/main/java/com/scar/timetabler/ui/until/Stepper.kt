package com.scar.timetabler.ui.until

import android.content.res.Configuration.UI_MODE_NIGHT_YES
import androidx.compose.animation.*
import androidx.compose.animation.core.Spring
import androidx.compose.animation.core.spring
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.draw.drawBehind
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.StrokeCap
import androidx.compose.ui.semantics.Role
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.scar.timetabler.ui.theme.TimeTablerTheme

data class Step(val title : String, val caption : String)
data class Snapshot(val previousIndex : Int  = -1, val nextIndex : Int = 0, val step : Step){
    fun isBack() = if(previousIndex == -1) true else nextIndex < previousIndex
}

@Composable
fun Stepper(
    modifier: Modifier = Modifier,
    steps: Array<Step>,
    state: Snapshot = Snapshot(-1, 0, Step("", "")),
    onChange: (value : Snapshot) -> Unit
) {
    val lineColor = Color.Gray
    Column(modifier) {
        Row(
            Modifier
                .fillMaxWidth()
                .height(64.dp), verticalAlignment = Alignment.CenterVertically) {
            val modifierLine = Modifier
                .weight(1f)
                .padding(horizontal = 8.dp)

            for ((index, value) in steps.withIndex()){
                StepItem(index + 1, state.step == value) {
                    if(state.nextIndex - index == 1 || state.nextIndex - index == -1)
                        onChange(Snapshot(state.nextIndex, index, value))
                }
                if((index + 1) < steps.size)
                    Line(modifierLine, lineColor)
            }
        }
    }
}


@Composable
private fun StepItem(index: Int, isActive : Boolean, onChange: () -> Unit){
    val animatedColor by animateColorAsState(
        if(isActive) MaterialTheme.colors.secondary else Color.DarkGray,
        spring(stiffness = Spring.StiffnessLow)
    )
    Box(
        Modifier
            .width(48.dp)
            .height(48.dp)
            .clip(CircleShape)
            .background(animatedColor)
            .clickable(role = Role.Tab) {
                onChange()
            }) {
        Text(index.toString(),
            modifier = Modifier.align(Alignment.Center),
            color= MaterialTheme.colors.onPrimary,
            style = MaterialTheme.typography.caption
        )
    }
}

@Composable
private fun Line(modifier: Modifier, color: Color){
    Box(modifier.drawBehind {
        val y = (size.height / 2.0f)
        drawLine(
            color,
            Offset(0f, y),
            Offset(size.width, y),
            12f,
            StrokeCap.Round,
        )
    }) {}
}

@Composable
private fun View() {
    var stepState by remember {
        mutableStateOf(Snapshot(-1, 0, Step(
            "Регистрация",
            "Введите ваши учётные данные"
        )))
    }
    TimeTablerTheme {
        Surface {
            Column {
                Stepper(steps = arrayOf(
                    Step(
                        "Регистрация",
                        "Введите ваши учётные данные"
                    ),
                    Step(
                        "Придумайте пароль",
                        "Придумайте надёжный пароль для вашей учетной записи"
                    ),
                    Step(
                        "Подтвердите номер телефона",
                        "Введите код из СМС которое мы отправили вам на указанный номер"
                    )
                ), state =  stepState){
                    stepState = it
                }

                Text(stepState.step.title, Modifier.align(Alignment.CenterHorizontally))
                Text(stepState.step.caption, Modifier.align(Alignment.CenterHorizontally))
            }
        }
    }
}

@Preview
@Composable
private fun Light() = View()

@Preview(uiMode = UI_MODE_NIGHT_YES)
@Composable
private fun Night() = View()